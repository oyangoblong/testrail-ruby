require 'simplecov'
SimpleCov.start do # Do not move. Must load before code under test.

end
require 'rspec'
require 'webmock/rspec'
require 'bundler/setup'
require 'testrail-ruby'


#ENV['TESTRAIL_BASE_URL']='FOO' #$TESTRAIL_BASE_URL

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  #  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
    config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
